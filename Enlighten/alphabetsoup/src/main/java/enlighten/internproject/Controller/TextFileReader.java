/**
 * TextFileReader.java - Depends on the input contract of:
 * First Line = grid dimensions nxm
 * Next Lines = n length
 * Remaining Lines = indeterminant length
 * 
 * Class reads a file and sets data pertaining to the word puzzle requirements
 *  @author Connor Stack
 */

package enlighten.internproject.Controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader {
    private static final int ROW_INDEX = 0;
    private static final int COLUMN_INDEX = 1;
    private static final int DIMENSION = 2;

    private int[] dimensions = new int[DIMENSION];
    private char[][] wordPuzzleGrid;
    private List<String> wordBank = new ArrayList<>();
    private boolean isFileExtracted;

    public TextFileReader() {
    }

    public TextFileReader(String file) {
        extractTxtFileContent(file);
    }

    public void extractTxtFileContent(String file) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String fileLine;
            if ((fileLine = bufferedReader.readLine()) != null) {
                setDimension(fileLine);
                initializePuzzleGrid();
                setAllPuzzleGridRows(bufferedReader);
                setWordBank(bufferedReader);
                isFileExtracted = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("There was either no file entered, or the file entered could not be read.");
        }
    }

    private void setDimension(String lineFromFile) {
        String[] dimensionLine = lineFromFile.split("x");
        dimensions[ROW_INDEX] = Integer.parseInt(dimensionLine[ROW_INDEX]);
        dimensions[COLUMN_INDEX] = Integer.parseInt(dimensionLine[COLUMN_INDEX]);
    }

    private void initializePuzzleGrid() {
        wordPuzzleGrid = new char[dimensions[ROW_INDEX]][dimensions[COLUMN_INDEX]];
    }

    private void setAllPuzzleGridRows(BufferedReader bufferedReader) throws IOException {
        for (int row = 0; row < dimensions[ROW_INDEX]; row++) {
            String fileLine = bufferedReader.readLine();
            setPuzzleGridRow(fileLine, row);
        }
    }

    private void setPuzzleGridRow(String fileLine, int row) {
        String[] gridRow = fileLine.split(" ");
        for (int rowElement = 0; rowElement < dimensions[COLUMN_INDEX]; rowElement++) {
            wordPuzzleGrid[row][rowElement] = gridRow[rowElement].charAt(0);
        }
    }

    private void setWordBank(BufferedReader bufferedReader) throws IOException {
        String fileLine;
        while ((fileLine = bufferedReader.readLine()) != null) {
            wordBank.add(fileLine);
        }
    }

    public boolean isFileExtracted() {
        return this.isFileExtracted;
    }

    public int[] getDimensions() {
        return this.dimensions;
    }

    public char[][] getWordPuzzleGrid() {
        return this.wordPuzzleGrid;
    }

    public List<String> getWordBank() {
        return this.wordBank;
    }
}