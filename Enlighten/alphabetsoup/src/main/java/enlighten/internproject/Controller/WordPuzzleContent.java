/**
 * WordPuzzleContent.java - serves as an intermediary between TextFileReader and WordPuzzleSolver. This class can be set
 * by other means besides a text file if needed.
 *  @author Connor Stack
 */

package enlighten.internproject.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordPuzzleContent {
    private static final int COORDINATES_LENGTH = 4;
    private Map<String, int[]> solvedWordMap;
    private int[] dimensions;
    private char[][] wordPuzzleGrid;
    private List<String> wordBank;
    private int rowCount;
    private int columnCount;

    public WordPuzzleContent() {
    }

    public WordPuzzleContent(TextFileReader textFileReader) {
        setWordPuzzleContentFromFileReader(textFileReader);
    }

    public void setWordPuzzleContentFromFileReader(TextFileReader textFileReader) {
        this.dimensions = textFileReader.getDimensions();
        this.wordPuzzleGrid = textFileReader.getWordPuzzleGrid();
        this.wordBank = textFileReader.getWordBank();
        setSolvedWordMap();
        setRowCount();
        setColumnCount();
    }

    
    public int[] getDimensions() {
        return this.dimensions;
    }
    
    public char[][] getWordPuzzleGrid() {
        return this.wordPuzzleGrid;
    }
    
    public List<String> getWordBank() {
        return this.wordBank;
    }

    public void setSolvedWordMap() {
        solvedWordMap = new HashMap<>();
        for (String word : wordBank) {
            this.solvedWordMap.put(word, new int[COORDINATES_LENGTH]);
        }
    }
    
    private void setRowCount() {
        this.rowCount = dimensions[0];
    }
    
    private void setColumnCount() {
        this.columnCount = dimensions[1];
    }
    
    public int getRowCount() {
        return this.rowCount;
    }

    public int getColumnCount() {
        return this.columnCount;
    }

    public void setDimensions(int[] dimensions) {
        this.dimensions = dimensions;
        setColumnCount(dimensions[0]);
        setRowCount(dimensions[1]);
    }

    public void setWordPuzzleGrid(char[][] wordPuzzleGrid) {
        this.wordPuzzleGrid = wordPuzzleGrid;
    }

    public void setWordBank(List<String> wordBank) {
        this.wordBank = wordBank;
    }

    private void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    private void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }
}
