/**
 * WordPuzzleSolver.java - takes a set WordPuzzleContent object and solves the puzzle based on the data given.
 * The class is expecting a char[][], List<String>, int, int in the object. This class could be modified to solve puzzles from
 * other sources if this contract is maintained.
 *  @author Connor Stack
 */

package enlighten.internproject.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import enlighten.internproject.Controller.WordPuzzleContent;

public class WordPuzzleSolver {
    private static final int TOTAL_DIRECTIONS = 8;
    private static int[] ROW_SHIFT = { 0, 1, 1, 1, 0, -1, -1, -1 };
    private static int[] COLUMN_SHIFT = { 1, 1, 0, -1, -1, -1, 0, 1 };
    private char[][] wordPuzzleGrid;
    private List<String> wordBank;
    private int[] foundCoordinates;
    private Map<String, int[]> solvedWordMap;
    private String keyword;
    private int rowCount;
    private int columnCount;
    private int rowIndex;
    private int columnIndex;

    public WordPuzzleSolver(WordPuzzleContent wordPuzzleContent) {
        this.wordPuzzleGrid = wordPuzzleContent.getWordPuzzleGrid();
        this.wordBank = wordPuzzleContent.getWordBank();
        this.rowCount = wordPuzzleContent.getRowCount();
        this.columnCount = wordPuzzleContent.getColumnCount();
    }

    public Map<String, int[]> solvePuzzle() {
        this.solvedWordMap = new HashMap<>();
        for (String keyWord : wordBank) {
            this.keyword = keyWord;
            iterateRows();

        }
        return this.solvedWordMap;
    }

    private void iterateRows() {
        for (this.rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            iterateRowIndicies();
        }
    }

    private void iterateRowIndicies() {
        for (this.columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            findWordStartingCoordinates();
        }
    }

    private void findWordStartingCoordinates() {
        char firstLetter = this.keyword.charAt(0);
        if (wordPuzzleGrid[this.rowIndex][this.columnIndex] == firstLetter) {
            foundCoordinates = findWordEndingCoordinates();
            if (foundCoordinates != null) {
                this.solvedWordMap.put(keyword, foundCoordinates);
            }
        }
    }

    private int[] findWordEndingCoordinates() {
        int keywordLength = this.keyword.length();

        for (int direction = 0; direction < TOTAL_DIRECTIONS; direction++) {
            int currentRow = this.rowIndex;
            int currentColumn = this.columnIndex;

            for (int keywordIndex = 0; keywordIndex < keywordLength; keywordIndex++) {
                if (isRowOutOfBounds(currentRow) || isColumnOutOfBounds(currentColumn)) {
                    break;
                }

                if (wordPuzzleGrid[currentRow][currentColumn] != keyword.charAt(keywordIndex)) {
                    break;
                }

                currentRow += ROW_SHIFT[direction];
                currentColumn += COLUMN_SHIFT[direction];

                if (keywordIndex == keywordLength - 1) {
                    foundCoordinates = new int[4];
                    foundCoordinates[0] = this.rowIndex;
                    foundCoordinates[1] = this.columnIndex;
                    foundCoordinates[2] = currentRow -= ROW_SHIFT[direction];
                    foundCoordinates[3] = currentColumn -= COLUMN_SHIFT[direction];
                    return foundCoordinates;
                }
            }
        }

        return null;
    }

    private boolean isRowOutOfBounds(int currentRow) {
        return (currentRow < 0 || currentRow >= this.rowCount);
    }

    private boolean isColumnOutOfBounds(int currentColumn) {
        return (currentColumn < 0 || currentColumn >= this.columnCount);
    }

    public Map<String, int[]> getSolvedWordMap() {
        return this.solvedWordMap;
    }

    public List<String> getWordBank() {
        return this.wordBank;
    }
}
