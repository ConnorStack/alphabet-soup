/**
 * Main.java - Takes a file as a command line argument, TextFileReader sets data to a WordPuzzleContent intermediary
 * which is used by WordPuzzleSolver to solve the puzzle (.solvePuzzle). DisplaySolvedPuzzle outputs data in the appropriate format
 * @author Connor Stack
 */
package enlighten.internproject;

import enlighten.internproject.Controller.TextFileReader;
import enlighten.internproject.Controller.WordPuzzleContent;
import enlighten.internproject.Model.WordPuzzleSolver;
import enlighten.internproject.View.DisplaySolvedPuzzle;

public class Main {
    private static final int FILENAME_ARG = 0;

    public static void main(String[] args) {
        String filename = args[FILENAME_ARG];

        TextFileReader textFileReader = new TextFileReader(filename);

        WordPuzzleContent wordPuzzleContent = new WordPuzzleContent(textFileReader);

        WordPuzzleSolver wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        wordPuzzleSolver.solvePuzzle();

        DisplaySolvedPuzzle displaySolvedPuzzle = new DisplaySolvedPuzzle(wordPuzzleSolver);
        displaySolvedPuzzle.displayWordPuzzleResults();
    }
}
