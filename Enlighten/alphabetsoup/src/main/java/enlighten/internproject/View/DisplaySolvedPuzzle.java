/**
 * DisplaySolvedPuzzle.java - Displays the solved puzzle data in the appropriate format
 *  @author Connor Stack
 */
package enlighten.internproject.View;

import enlighten.internproject.Model.WordPuzzleSolver;

public class DisplaySolvedPuzzle {
    private WordPuzzleSolver wordPuzzleSolver;

    public DisplaySolvedPuzzle(WordPuzzleSolver solvedPuzzle) {
        this.wordPuzzleSolver = solvedPuzzle;
    }

    public void displayWordPuzzleResults() {
        for (String keyWord : wordPuzzleSolver.getWordBank()) {
            int[] arr = wordPuzzleSolver.getSolvedWordMap().get(keyWord);
            if(arr != null){
                System.out.println(keyWord + " " + arr[0] + ":" + arr[1] + " " + arr[2] + ":" + arr[3]);
            }
        }
    }

}
