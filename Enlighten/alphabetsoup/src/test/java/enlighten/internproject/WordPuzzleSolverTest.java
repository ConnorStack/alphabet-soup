package enlighten.internproject;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import enlighten.internproject.Controller.WordPuzzleContent;
import enlighten.internproject.Model.WordPuzzleSolver;

public class WordPuzzleSolverTest {
    WordPuzzleContent wordPuzzleContent;
    WordPuzzleSolver wordPuzzleSolver;

    @BeforeEach
    void setUp() {
        this.wordPuzzleContent = new WordPuzzleContent();
    }

    @Test
    void testSolvePuzzleWithHorizontalWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'X', 'X', 'X', 'X', 'X' },
                { 'X', 'O', 'N', 'E', 'X' },
                { 'X', 'X', 'T', 'W', 'O' },
                { 'T', 'H', 'R', 'E', 'E' },
                { 'X', 'F', 'O', 'U', 'R' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 1, 1, 1, 3 });
        expectedSolvedWordMap.put("TWO", new int[] { 2, 2, 2, 4 });
        expectedSolvedWordMap.put("THREE", new int[] { 3, 0, 3, 4 });
        expectedSolvedWordMap.put("FOUR", new int[] { 4, 1, 4, 4 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithHorizontalBackwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'X', 'X', 'X', 'X', 'X' },
                { 'X', 'E', 'N', 'O', 'X' },
                { 'X', 'X', 'O', 'W', 'T' },
                { 'E', 'E', 'R', 'H', 'T' },
                { 'X', 'R', 'U', 'O', 'F' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 1, 3, 1, 1 });
        expectedSolvedWordMap.put("TWO", new int[] { 2, 4, 2, 2 });
        expectedSolvedWordMap.put("THREE", new int[] { 3, 4, 3, 0 });
        expectedSolvedWordMap.put("FOUR", new int[] { 4, 4, 4, 1 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithVerticalWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'O', 'X', 'T', 'F', 'X' },
                { 'N', 'T', 'H', 'O', 'X' },
                { 'E', 'W', 'R', 'U', 'X' },
                { 'X', 'O', 'E', 'R', 'X' },
                { 'X', 'X', 'E', 'X', 'X' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 0, 0, 2, 0 });
        expectedSolvedWordMap.put("TWO", new int[] { 1, 1, 3, 1 });
        expectedSolvedWordMap.put("THREE", new int[] { 0, 2, 4, 2 });
        expectedSolvedWordMap.put("FOUR", new int[] { 0, 3, 3, 3 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithVerticalBackwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'E', 'X', 'E', 'R', 'X' },
                { 'N', 'O', 'E', 'U', 'X' },
                { 'O', 'W', 'R', 'O', 'X' },
                { 'X', 'T', 'H', 'F', 'X' },
                { 'X', 'X', 'T', 'X', 'X' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 2, 0, 0, 0 });
        expectedSolvedWordMap.put("TWO", new int[] { 3, 1, 1, 1 });
        expectedSolvedWordMap.put("THREE", new int[] { 4, 2, 0, 2 });
        expectedSolvedWordMap.put("FOUR", new int[] { 3, 3, 0, 3 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithDiagonalDownwardsForwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'T', 'F', 'X', 'X', 'X' },
                { 'T', 'H', 'O', 'X', 'X' },
                { 'O', 'W', 'R', 'U', 'X' },
                { 'X', 'N', 'O', 'E', 'R' },
                { 'X', 'X', 'E', 'X', 'E' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 2, 0, 4, 2 });
        expectedSolvedWordMap.put("TWO", new int[] { 1, 0, 3, 2 });
        expectedSolvedWordMap.put("THREE", new int[] { 0, 0, 4, 4 });
        expectedSolvedWordMap.put("FOUR", new int[] { 0, 1, 3, 4 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithDiagonalDownwardsBackwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'E', 'R', 'X', 'X', 'X' },
                { 'O', 'E', 'U', 'X', 'X' },
                { 'E', 'W', 'R', 'O', 'X' },
                { 'X', 'N', 'T', 'H', 'F' },
                { 'X', 'X', 'O', 'X', 'T' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 4, 2, 2, 0 });
        expectedSolvedWordMap.put("TWO", new int[] { 3, 2, 1, 0 });
        expectedSolvedWordMap.put("THREE", new int[] { 4, 4, 0, 0 });
        expectedSolvedWordMap.put("FOUR", new int[] { 3, 4, 0, 1 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithDiagonalUpwardsForwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'X', 'X', 'E', 'X', 'E' },
                { 'X', 'N', 'O', 'E', 'R' },
                { 'O', 'W', 'R', 'U', 'X' },
                { 'T', 'H', 'O', 'X', 'X' },
                { 'T', 'F', 'X', 'X', 'X' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 2, 0, 0, 2 });
        expectedSolvedWordMap.put("TWO", new int[] { 3, 0, 1, 2 });
        expectedSolvedWordMap.put("THREE", new int[] { 4, 0, 0, 4 });
        expectedSolvedWordMap.put("FOUR", new int[] { 4, 1, 1, 4 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    @Test
    void testSolvePuzzleWithDiagonalUpwardsBackwardsWords() {
        int[] dimensions = { 5, 5 };
        List<String> wordBank = Arrays.asList("ONE", "TWO", "THREE", "FOUR");
        char[][] forwardWords = {
                { 'X', 'X', 'O', 'X', 'T' },
                { 'X', 'N', 'T', 'H', 'F' },
                { 'E', 'W', 'R', 'O', 'X' },
                { 'O', 'E', 'U', 'X', 'X' },
                { 'E', 'R', 'X', 'X', 'X' }
        };
        setWordPuzzleContent(wordPuzzleContent, dimensions, forwardWords, wordBank);

        Map<String, int[]> expectedSolvedWordMap = new HashMap<>();
        expectedSolvedWordMap.put("ONE", new int[] { 0, 2, 2, 0 });
        expectedSolvedWordMap.put("TWO", new int[] { 1, 2, 3, 0 });
        expectedSolvedWordMap.put("THREE", new int[] { 0, 4, 4, 0 });
        expectedSolvedWordMap.put("FOUR", new int[] { 1, 4, 4, 1 });

        this.wordPuzzleSolver = new WordPuzzleSolver(wordPuzzleContent);
        Map<String, int[]> testSolvedWordMap = wordPuzzleSolver.solvePuzzle();

        for (Map.Entry<String, int[]> expectedEntry : expectedSolvedWordMap.entrySet()) {
            String word = expectedEntry.getKey();
            int[] expectedCoordinates = expectedEntry.getValue();
            int[] actualCoordinates = testSolvedWordMap.get(word);

            assertNotNull(actualCoordinates);
            assertArrayEquals(expectedCoordinates, actualCoordinates);
        }
    }

    private void setWordPuzzleContent(WordPuzzleContent wordPuzzleContent, int[] dimensions, char[][] forwardWords,
            List<String> wordBank) {
        wordPuzzleContent.setDimensions(dimensions);
        wordPuzzleContent.setWordPuzzleGrid(forwardWords);
        wordPuzzleContent.setWordBank(wordBank);
        wordPuzzleContent.setSolvedWordMap();
    }
}
