Connor Stack's Alphabet-Soup Implementation

This project accepts a .txt file as a command line argument.

I used Maven as a build tool and used the commands...

mvn compile
mvn exec:java -Dexec.mainClass=enlighten.internproject.Main -Dexec.args="example.txt"

...to run the program.
